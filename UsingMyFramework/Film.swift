//
//  Film.swift
//  UsingMyFramework
//
//  Created by Hưng Vũ on 31/08/2021.
//  Copyright © 2021 Nazario Mariano. All rights reserved.
//

import Foundation

struct GenKeyResponse : Codable {
    var data: DataGenKeyResponse
    var msg: String
    
}

struct DataGenKeyResponse : Codable {
    var items: [ItemVector]
    var encode_key: String
}
struct ItemVector : Codable {
    var vector: Array<Float>
   
}

struct RequestVerify: Codable {
    var items: [ItemVector]
}

struct VerifyResponse : Codable {
    var data: DataVerifyResponse
    var msg: String
    
}
struct DataVerifyResponse : Codable {
    var utt: Double
    var verify: Bool
}
